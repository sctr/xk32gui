#include <gtkmm.h>

#include "sctr_frm.h"
#include "sctr_timer.h"
#include "sctr_serial.h"
#include "sctr_data.h"
#include <libserial/SerialStream.h>
#include <libserial/SerialPort.h>

#include <unistd.h>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <thread>
#include <mutex>

using namespace std;
using namespace LibSerial;

/*main frame*/
sctr_frm o_sctr;

/*serial & data*/
sctr_data o_sb_data;
sctr_serial o_sb_serial("/dev/ttyUSB0");

int main(int argc, char **argv){

  // TODO:: thread for every HBox eg. pmsm, akku, debug, sim
  // TODO:: get uart ready for distribute to HBoxes
  // thread t_serial = o_sb_serial.run(&o_sb_data);
  // thread t_frm_run = o_frm.run(&o_sb_data);
  
  thread t_frm_init(&sctr_frm::init, &o_sctr);
  thread t_frm_run(&sctr_frm::run, &o_sctr,  &o_sb_data);
  thread t_serial(&sctr_serial::run, &o_sb_serial, &o_sb_data);
  
  if (t_frm_init.joinable()){
    cout << "frm_init joinable" << endl;
    t_frm_init.join();
  }
  else
  {
    cout << "frm_init NOT joinable" << endl;
  }

  if (t_frm_run.joinable()){
    cout << "frm_run joinable" << endl;
    t_frm_run.join();    
  }
  else{
    cout << "frm_run NOT joinable" << endl;
  }

  if (t_serial.joinable()){
    cout << "serial joinable" << endl;
    t_serial.join();
  }
  else{
    cout << "serial NOT joinable" << endl;
  }

  return 0;
}
