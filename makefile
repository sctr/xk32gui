CC=g++			#CMD Compiler cpp
RM=rm			#CMD Remove

P_W_ALL= -Wall	#Parameter Warning All
P_VERB= -v		#Parameter Verbose
P_OUT= -o		#Parameter Out
P_JOBS4= -j4	#Parameter compiling processes
LINK=-Wl,--start-group -lm -Wl,--end-group -Wl,--gc-sections

#FILE_MAIN= sctr_main.cc
PKG_CONF= `pkg-config gtkmm-3.0 libserial --cflags --libs`

SRC_DIR=src/
INC_DIR=inc
OBJ_DIR=obj/
DBG_DIR=debug/

OUTPUT=main
OMAIN=main.o
CMAIN=main.cc
SRC=$(wildcard $(SRC_DIR)*.cc)
OBJ=$(SRC:$(SRC_DIR)%.cc=$(OBJ_DIR)%.o)
INCLUDES=-I$(INC_DIR)

#cc: $(DIR_SRC)$(OUTPUT)cc
#	$(CC) $(P_W_ALL) $(MAIN) $(DIR_SRC)sctr_frm.cc $(DIR_SRC)sctr_rotor_area.cc $(DIR_SRC)sctr_timer.cc $(DIR_SRC)sctr_serial.cc -o $(DIR_DBG)$(OUTPUT) $(PKG_CONF)

all:	$(OUTPUT)

$(OMAIN): $(CMAIN)
	$(CC) $(P_W_ALL) $(INCLUDES) -c $^ -o $@ $(PKG_CONF)
	@echo "-------------\n" $@ "build\n-------------"

$(OUTPUT): $(SRC)
	$(CC) $(P_W_ALL) $(INCLUDES) $^ main.cc -o $@ $(PKG_CONF)
	@echo "-------------\n" $@ "build\n-------------"

clean: 
	$(RM) main
