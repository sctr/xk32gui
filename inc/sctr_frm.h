#ifndef SCTR_FRM_H
#define SCTR_FRM_H

#include <gtkmm.h>
#include <thread>
#include <iostream>
#include "sctr_data.h"
#include "hbox_pmsm_monitor.h"
#include "hbox_debug.h"
#include "hbox_simulation.h"
#include "hbox_akku.h"

class sctr_frm {
  
public:
  
  sctr_frm();
  virtual ~sctr_frm();

  //App
  Glib::RefPtr <Gtk::Application> o_app = Gtk::Application::create("xk32Gui");
  
  //Window
  Gtk::Window o_frm;
    
  //NOTEBOOKS
  Gtk::Notebook o_notebook_frm;

  //hboxes for Notebook tabs
  hbox_pmsm_monitor o_hbox_pmsm_monitor;
  hbox_debug o_hbox_debug;
  hbox_simulation o_hbox_sim;
  hbox_akku o_hbox_akku;

  void init();
  void run(sctr_data *pdata);
  //signal handler
  void on_button_clicked();

private:
  
protected:
    
};

#endif //SCTR_FRM_H
