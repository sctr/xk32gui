#ifndef SCTR_TIMER_H
#define SCTR_TIMER_H

#include <gtkmm.h>
//#include <glibmm/main.h>
#include <iostream>
#include <map>
#include "sctr_rotor_area.h"


class sctr_timer: public Gtk::Window{
 public:
  sctr_timer();
  virtual ~sctr_timer();
 protected:
  bool on_timeout_rotor(int timeout_number);
  int tim_num;
  int count_value;
  const int timeout_value;

  //STL map for storing our connection
  std::map<int, sigc::connection> timer_rotor;
  //STL map for storing timer values
  std::map<int, int> counter_rotor;
  
};

#endif //SCTR_TIMER_H
