#ifndef SCTR_ROTOR_AREA_H
#define SCTR_ROTOR_AREA_H

#include <gtkmm/drawingarea.h>

namespace rotor{

class rotor_area : public Gtk::DrawingArea{

 public:
  rotor_area();
  virtual ~rotor_area();

  struct config_rotor_area{

  };
  int get_ctr_rotor_area(void){
	  return ctr_rotor_area++;
  }



 private:
  bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;
  int ctr_rotor_area;
};
} //namespace rotor

#endif //SCTR_ROTOR_AREA_H
