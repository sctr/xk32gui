#ifndef SCTR_DATA_H
#define SCTR_DATA_H

#include <vector>
#include <string>
#include <glibmm.h>

#include <mutex>

using namespace std;

class sctr_data {

 private:
  
  vector <string> data;
  
 public:

  sctr_data();
  ~sctr_data();
  
  void set_data(const Glib::ustring &text);
  Glib::ustring get_data(void);
  
  bool is_ntc0_available(void);
  void clear_ntc0(void);
  bool is_poti_available(void);
  void clear_poti(void);
  bool is_v__u_available(void);
  void clear_v__u(void);
  void sort();

  bool is_transfer_available(void);
  void set_transfer_data(Glib::ustring data_transfer);
  Glib::ustring get_transfer_data(void);
  void clear_transfer(void);

  /* TODO:: get list of variables from uC */
  Glib::ustring buffer;
  bool b_ntc0_available;
  Glib::ustring ntc0;
  bool b_poti_available;
  Glib::ustring poti;
  bool b_v__u_available;
  Glib::ustring v__u;

  Glib::ustring transfer_data;
  bool b_transfer_available;
  
  mutex mtx_serial_data;
};

#endif // SCTR_DATA_H
