
#ifndef SCTR_SERIAL_H
#define SCTR_SERIAL_H

#include <gtkmm.h>
#include <libserial/SerialStream.h>
#include <libserial/SerialPort.h>
#include <iostream>
#include <cstdlib>
#include <string.h>
#include <thread>
#include <mutex>

#include "sctr_data.h"

using namespace LibSerial;
using namespace std;

class sctr_serial : public SerialPort{

private:

  void sort_to_list();
  Glib::ustring transfer_data;
  bool b_transfer_data_available;
  
public:
  
  sctr_serial( const std::string& serialPortName);
  virtual ~sctr_serial();
  
  void run(sctr_data *pdata);
  void send_to_sb(unsigned char);
  void receive(sctr_data *pdata);

};

#endif
