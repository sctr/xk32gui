#ifndef HBOX_PMSM_MONITOR_H
#define HBOX_PMSM_MONITOR_H

#include <gtkmm.h>
#include <signal.h>
#include "sctr_data.h"
#include <iostream>
class hbox_pmsm_monitor : public Gtk::HBox{

 public:
  //constructor destructor
  hbox_pmsm_monitor();
  virtual ~hbox_pmsm_monitor();
  
  Gtk::VBox vbox_pmsm_lbl, vbox_pmsm_entry, vbox_pmsm_lbl_out, vbox_pmsm_entry_out;

  Gtk::Label lbl_pmsm_ntc0, lbl_pmsm_ntc5, lbl_pmsm_poti, lbl_pmsm_uCTp, \
    lbl_pmsm_iu, lbl_pmsm_iv, lbl_pmsm_iw, lbl_pmsm_out;

  Gtk::Entry entry_pmsm_ntc0, entry_pmsm_ntc5,	\
    entry_pmsm_poti, entry_pmsm_uCTp,		\
    entry_pmsm_iu, entry_pmsm_iv,	     \
    entry_pmsm_iw, entry_pmsm_out;
  
  void setNtc0Entry(const Glib::ustring text);
  void setPotiEntry(const Glib::ustring text);
  void setVUEntry(const Glib::ustring text);
  bool isTransferDataAvailable(void);
  Glib::ustring getTransferData(void);
  
 protected:
  void onTransferInput(void);
  
 private:
  Glib::ustring transfer_data;
  bool b_transfer_data_available;
  
};

void clicked(Gtk::EntryIconPosition icon_pos);

#endif //HBOX_PMSM_MONITOR_H
