#ifndef HBOX_SIMULATION_H
#define HBOX_SIMULATION_H

#include <gtkmm.h>
#include "sctr_rotor_area.h"

class hbox_simulation:public Gtk::HBox{

 private:

  Gtk::VBox vbox_sim_param, vbox_sim_param_lbl, vbox_sim_param_scale,
    vbox_sim_rotor, vbox_sim_values;
  Gtk::Label lbl_sim_speed, lbl_sim_torque;
  Gtk::ComboBox cb_sim;
  Glib::RefPtr<Gtk::Adjustment> adj_scale_speed, adj_scale_torque;
  Gtk::Scale scale_speed, scale_torque;
  rotor::rotor_area rotor;
  
 public:
  hbox_simulation();
  virtual ~hbox_simulation();

};

#endif //HBOX_SIMULATION_H
