#ifndef HBOX_DEBUG_H
#define HBOX_DEBUG_H

#include <gtkmm.h>

class hbox_debug:public Gtk::HBox{

 private:
  
  Gtk::VBox vbox_debug_selection, vbox_debug_tv_output;
  Gtk::CheckButton chb_debug_chb1, chb_debug_chb2;
  Gtk::Label lbl_debug_lbl1, lbl_debug_lbl2;
  Gtk::ComboBoxText cbt_debug_connection, cbt_debug_speed;
  Gtk::ListBox lb_debug_testselect;
  Gtk::RadioButton rb_debug_rb1, rb_debug_rb2, rb_debug_rb3;
  Gtk::TextView tv_debug_output;
  Gtk::Allocation alloc_vbox_debug_selection;
  
 public:
  hbox_debug();
  virtual ~hbox_debug();

};

#endif //HBOX_DEBUG_H
