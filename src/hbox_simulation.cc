#include "hbox_simulation.h"


//constructor
hbox_simulation::hbox_simulation():
  vbox_sim_param(Gtk::ORIENTATION_VERTICAL, 0),
  lbl_sim_speed("Speed", false),
  lbl_sim_torque("Torque", false),
  adj_scale_speed(Gtk::Adjustment::create(0.0, 0.0, 101.0, 0.1, 1.0, 1.0)),
  adj_scale_torque(Gtk::Adjustment::create(0.0, 0.0, 101.0, 0.1, 1.0, 1.0)),
  scale_speed(adj_scale_speed, Gtk::ORIENTATION_VERTICAL),
  scale_torque(adj_scale_torque, Gtk::ORIENTATION_VERTICAL)
{
  
  scale_speed.set_digits(1);
  scale_speed.set_value_pos(Gtk::POS_TOP);
  scale_speed.set_draw_value();
  scale_speed.set_inverted();

  scale_torque.set_digits(1);
  scale_torque.set_value_pos(Gtk::POS_TOP);
  scale_torque.set_draw_value();
  scale_torque.set_inverted();
    
  /*PMSM Simulation Tab Packing*/
  pack_start(vbox_sim_param);
  pack_start(vbox_sim_rotor);
  pack_start(vbox_sim_values);
  vbox_sim_param.pack_start(vbox_sim_param_scale);
  vbox_sim_param.pack_start(vbox_sim_param_lbl);
  vbox_sim_param_scale.pack_start(scale_speed);
  vbox_sim_param_scale.pack_start(scale_torque);
  vbox_sim_param_lbl.pack_start(lbl_sim_speed);
  vbox_sim_param_lbl.pack_start(lbl_sim_torque);
  vbox_sim_rotor.pack_start(rotor);

}

//destructor
hbox_simulation::~hbox_simulation(){
  
}
