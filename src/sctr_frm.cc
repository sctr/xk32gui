#include "sctr_frm.h"

sctr_frm::sctr_frm()
{

  /*PMSM monitor add*/
  o_notebook_frm.append_page(o_hbox_pmsm_monitor, "PMSM Monitor");
  o_notebook_frm.append_page(o_hbox_akku, "Akkumulator Monitor");
  o_notebook_frm.append_page(o_hbox_debug, "Debug");
  o_notebook_frm.append_page(o_hbox_sim, "PMSM Simulation");

  /*Frame set*/
  o_frm.set_title("XK control panel");
  o_frm.add(o_notebook_frm);
  o_frm.set_default_size(1000, 1000);
  o_frm.show_all_children();
  
}

sctr_frm::~sctr_frm(){
  
}

void sctr_frm::init(){
  o_app->run(o_frm);
}

void sctr_frm::run(sctr_data *pdata){

  Glib::ustring ntc0_data;
  Glib::ustring poti_data;
  Glib::ustring v__u_data;
  
  while(1){

    pdata->mtx_serial_data.lock();
    if(pdata->is_ntc0_available() == true){
      ntc0_data =  pdata->ntc0.substr(0,4);
      this->o_hbox_pmsm_monitor.setNtc0Entry(ntc0_data);
    }
    
    if(pdata->is_poti_available() == true){
      poti_data =  pdata->poti.substr(0,4);
      this->o_hbox_pmsm_monitor.setPotiEntry(poti_data);
    }

    if(pdata->is_v__u_available() == true){
      v__u_data =  pdata->v__u.substr(0,4);
      this->o_hbox_pmsm_monitor.setVUEntry(v__u_data);
    }

    if(this->o_hbox_pmsm_monitor.isTransferDataAvailable()){
      pdata->pset_transfer_data(this->o_hbox_pmsm_monitor.getTransferData());
    }
    pdata->mtx_serial_data.unlock();
  }

}
