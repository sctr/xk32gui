#include "sctr_data.h"

sctr_data::sctr_data(){

  mtx_serial_data.unlock();
  b_ntc0_available = false;
  b_poti_available = false;
  b_v__u_available = false;
  
}

sctr_data::~sctr_data(){
  
}

void sctr_data::set_data(const Glib::ustring &text){
  data.push_back(text);
}

bool sctr_data::is_ntc0_available(){
  bool available = false;
  available = this->b_ntc0_available;
  if (available == true){
    this->clear_ntc0();
  }
  return available;
}

bool sctr_data::is_poti_available(){
  bool available = false;
  available = this->b_poti_available;
  if (available == true){
    this->clear_poti();
  }
  return available;
}

bool sctr_data::is_v__u_available(){
  bool available = false;
  available = this->b_v__u_available;
  if (available == true){
    this->clear_v__u();
  }
  return available;
}

bool sctr_data::is_transfer_available(){
  bool available = false;
  available = this->b_transfer_available;
  return available;
}

Glib::ustring sctr_data::get_transfer_data(){
  Glib::ustring tranfer_data = this->transfer_data;
  this->clear_transfer();
  return transfer_data;
}

void sctr_data::set_transfer_data(Glib::ustring data_transfer){
  this->transfer_data = data_transfer;
  this->b_transfer_available = true;
}

void sctr_data::clear_ntc0(void){
  this->b_ntc0_available = false;
}

void sctr_data::clear_poti(void){
  this->b_poti_available = false;
}

void sctr_data::clear_v__u(void){
  this->b_v__u_available = false;
}

Glib::ustring sctr_data::get_data(void){

  return data.front();  
}

void sctr_data::clear_transfer(void){
  this->transfer_data.erase();
}


void sctr_data::sort(){
  /*sort incomming values due to mc compilation txt output*/
  
}
