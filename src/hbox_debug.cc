#include "hbox_debug.h"


//constructor
hbox_debug::hbox_debug():
  vbox_debug_selection(Gtk::ORIENTATION_VERTICAL, 10),
  vbox_debug_tv_output(Gtk::ORIENTATION_VERTICAL, 10),
  chb_debug_chb1("Checkbox1"),
  chb_debug_chb2("Checkbox2"),
  rb_debug_rb1("Radiobutton1"),
  rb_debug_rb2("Radiobutton2"),
  rb_debug_rb3("Radiobutton3")
{

  /*Debug Monitor Tab Configuration*/
  cbt_debug_connection.insert(0,"USB");
  cbt_debug_connection.insert(1,"COM");
  
  cbt_debug_speed.insert(1,"9600");
  cbt_debug_speed.insert(2,"48600");
  cbt_debug_speed.insert(3,"1000000");
  cbt_debug_speed.insert(3,"2000000");

  /*Debug Monitor Tab Size*/
  alloc_vbox_debug_selection.set_width(10);
  alloc_vbox_debug_selection.set_height(100);
  vbox_debug_selection.size_allocate(alloc_vbox_debug_selection);

  /*Debug Monitor Tab Packing*/
  pack_start(vbox_debug_selection);
  pack_start(vbox_debug_tv_output);
  vbox_debug_selection.pack_start(cbt_debug_connection);
  vbox_debug_selection.pack_start(cbt_debug_speed);
  vbox_debug_selection.pack_start(chb_debug_chb1);
  vbox_debug_selection.pack_start(chb_debug_chb2);
  vbox_debug_selection.pack_start(lb_debug_testselect);
  vbox_debug_selection.pack_start(rb_debug_rb1);
  vbox_debug_selection.pack_start(rb_debug_rb2);
  vbox_debug_selection.pack_start(rb_debug_rb3);
  vbox_debug_tv_output.pack_start(tv_debug_output);
  rb_debug_rb2.join_group(rb_debug_rb1);
  rb_debug_rb3.join_group(rb_debug_rb2);

}

//destructor
hbox_debug::~hbox_debug(){
  
}
