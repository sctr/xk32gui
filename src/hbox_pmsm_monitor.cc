#include "hbox_pmsm_monitor.h"

void clicked(Gtk::EntryIconPosition icon_pos){
  cout << "some" << endl;
}

hbox_pmsm_monitor::hbox_pmsm_monitor(){

  /*PMSM Monitor Tab Configuration*/
  lbl_pmsm_ntc0.set_text("NTC0");
  lbl_pmsm_ntc5.set_text("NTC5");
  lbl_pmsm_poti.set_text("Poti");
  lbl_pmsm_uCTp.set_text("uCTp");
  lbl_pmsm_iu.set_text("I__U");
  lbl_pmsm_iv.set_text("I__V");
  lbl_pmsm_iw.set_text("I__W");
  lbl_pmsm_out.set_text("Transfer:");

  /*PMSM Monitor Tab Packing*/
  pack_start(vbox_pmsm_lbl, false, false, 0);
  pack_start(vbox_pmsm_entry, false, false, 0);
  pack_start(vbox_pmsm_lbl_out, false, false, 0);
  pack_start(vbox_pmsm_entry_out, false, false, 0);
  
  vbox_pmsm_lbl.pack_start(lbl_pmsm_ntc0);
  vbox_pmsm_lbl.pack_start(lbl_pmsm_ntc5);
  vbox_pmsm_lbl.pack_start(lbl_pmsm_poti);
  vbox_pmsm_lbl.pack_start(lbl_pmsm_uCTp);
  vbox_pmsm_lbl.pack_start(lbl_pmsm_iu);
  vbox_pmsm_lbl.pack_start(lbl_pmsm_iv);
  vbox_pmsm_lbl.pack_start(lbl_pmsm_iw);
  
  vbox_pmsm_entry.pack_start(entry_pmsm_ntc0);
  vbox_pmsm_entry.pack_start(entry_pmsm_ntc5);
  vbox_pmsm_entry.pack_start(entry_pmsm_poti);
  vbox_pmsm_entry.pack_start(entry_pmsm_uCTp);
  vbox_pmsm_entry.pack_start(entry_pmsm_iu);
  vbox_pmsm_entry.pack_start(entry_pmsm_iv);
  vbox_pmsm_entry.pack_start(entry_pmsm_iw);

  vbox_pmsm_lbl_out.pack_start(lbl_pmsm_out);
  vbox_pmsm_entry_out.pack_start(entry_pmsm_out);

  /* TODO:: find out why sigc will not connect with mem_fun() and ptr_fun()*/
  //  entry_pmsm_out.signal_icon_press().connect(sigc::mem_fun(*this, &hbox_pmsm_monitor::onTransferInput));
  //entry_pmsm_out.signal_icon_press().connect(sigc::ptr_fun(&clicked(*this)));
  
  transfer_data = "";
  b_transfer_data_available = false;
}

hbox_pmsm_monitor:: ~hbox_pmsm_monitor(){
  
}

void hbox_pmsm_monitor::setNtc0Entry(const Glib::ustring text){
  //todo read all data of vector where serial writes in
  //and put it in the referred entries
  entry_pmsm_ntc0.set_text(text);
}

void hbox_pmsm_monitor::setPotiEntry(const Glib::ustring text){
  //todo read all data of vector where serial writes in
  //and put it in the referred entries
  entry_pmsm_poti.set_text(text);
}

void hbox_pmsm_monitor::setVUEntry(const Glib::ustring text){
  //todo read all data of vector where serial writes in
  //and put it in the referred entries
  entry_pmsm_iv.set_text(text);
}

void hbox_pmsm_monitor::onTransferInput(void){
  /*send command over uart tranfer*/
  this->transfer_data = this->entry_pmsm_out.get_text();
  this->b_transfer_data_available = true;  
}

bool hbox_pmsm_monitor::isTransferDataAvailable(void){
  return this->b_transfer_data_available;
}
Glib::ustring hbox_pmsm_monitor::getTransferData(void){
  this->b_transfer_data_available = false;
  return (this->transfer_data);
  
}
