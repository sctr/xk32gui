#include "sctr_timer.h"

sctr_timer::sctr_timer() :
		tim_num(0), count_value(0), timeout_value(200)

{
	sigc::slot<bool> time_slot = sigc::bind(
			sigc::mem_fun(*this, &sctr_timer::on_timeout_rotor), tim_num);
	sigc::connection conn = Glib::signal_timeout().connect(time_slot,
			timeout_value);
	timer_rotor[tim_num] = conn;
	counter_rotor[tim_num] = count_value + 1;
	std::cout << "Added timeout " << tim_num++ << std::endl;
}
sctr_timer::~sctr_timer() {
}

bool sctr_timer::on_timeout_rotor(int timer_number) {
//	std::cout << "Timer " << timer_number << " fired..." << std::endl;
	//TODO: implement the external function wich has access to a variable to this timer call
	//int ctr_ext = get_ctr_rotor_area();
//std::cout<<"Count " << ctr_ext << std::endl;
	return true;
}
